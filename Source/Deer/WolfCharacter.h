// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AnimalActions.h"
#include "GameFramework/CharacterMovementComponent.h" 
#include "WolfCharacter.generated.h"

UCLASS()
class DEER_API AWolfCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AWolfCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Input)
		float TurnRateGamepad;

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);


public:

	UPROPERTY(BlueprintReadWrite)
	EAnimalAction EWolfActions;


	UFUNCTION(BlueprintCallable)
	void SetMaxSpeed(float newSpeed) { GetCharacterMovement()->MaxWalkSpeed = newSpeed; };

	UPROPERTY(BlueprintReadWrite)
		bool IsRunningAway;
	UPROPERTY(BlueprintReadWrite)
		bool IsWalking;
	UPROPERTY(BlueprintReadWrite)
		bool IsIdle;
	UPROPERTY(BlueprintReadWrite)
		bool IsGlaring;
	UPROPERTY(BlueprintReadWrite)
		bool IsEating;
	UPROPERTY(BlueprintReadWrite)
		bool IsAttacking;
	UPROPERTY(BlueprintReadWrite)
		bool IsResting;
	UPROPERTY(BlueprintReadWrite)
		bool IsSleeping;

};
