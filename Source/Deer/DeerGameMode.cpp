// Copyright Epic Games, Inc. All Rights Reserved.

#include "DeerGameMode.h"
#include "DeerCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADeerGameMode::ADeerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
