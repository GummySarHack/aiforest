// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "DeerCharacter.h"
#include "DeerAIController.generated.h"

/**
 * 
 */
UCLASS()
class DEER_API ADeerAIController : public AAIController
{
	GENERATED_BODY()
	

public:

	ADeerAIController();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UAIPerceptionComponent* AIPerceptionComponent;

public:

	/* VARIABLES */
		
	UPROPERTY(BlueprintReadWrite)
	bool Loop = false;

	UPROPERTY(BlueprintReadWrite)
	ADeerCharacter* Deer;

	UPROPERTY(BlueprintReadWrite)
	AActor* DetectedPlayer;
	
};
