// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "DeerCharacter.h"
#include "WolfAIController.generated.h"

/**
 * 
 */
UCLASS()
class DEER_API AWolfAIController : public AAIController
{
	GENERATED_BODY()
	

public:

	AWolfAIController();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UAIPerceptionComponent* AIPerceptionComponent;


	/* VARIABLES */

	UPROPERTY(BlueprintReadWrite)
		bool Loop = false;

	UPROPERTY(BlueprintReadWrite)
		ADeerCharacter* Deer;

	UPROPERTY(BlueprintReadWrite)
		AActor* DetectedPlayer;
};
