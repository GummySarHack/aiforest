#pragma once

#include "CoreMinimal.h"
#include "AnimalActions.generated.h"

UENUM(BlueprintType)
enum class EAnimalAction : uint8
{
	EATING,
	WALKING,
	RUNNING,
	IDLE,
	ATTACKING,
	DIE,
	RESTING,
	SLEEPING,
	GLARE
};