// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DEER_AnimalActions_generated_h
#error "AnimalActions.generated.h already included, missing '#pragma once' in AnimalActions.h"
#endif
#define DEER_AnimalActions_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Deer_Source_Deer_AnimalActions_h


#define FOREACH_ENUM_EANIMALACTION(op) \
	op(EAnimalAction::EATING) \
	op(EAnimalAction::WALKING) \
	op(EAnimalAction::RUNNING) \
	op(EAnimalAction::IDLE) \
	op(EAnimalAction::ATTACKING) \
	op(EAnimalAction::DIE) \
	op(EAnimalAction::RESTING) \
	op(EAnimalAction::SLEEPING) \
	op(EAnimalAction::GLARE) 

enum class EAnimalAction : uint8;
template<> DEER_API UEnum* StaticEnum<EAnimalAction>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
