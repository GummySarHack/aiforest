// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Deer/WolfCharacter.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWolfCharacter() {}
// Cross Module References
	DEER_API UClass* Z_Construct_UClass_AWolfCharacter_NoRegister();
	DEER_API UClass* Z_Construct_UClass_AWolfCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_Deer();
	DEER_API UEnum* Z_Construct_UEnum_Deer_EAnimalAction();
// End Cross Module References
	DEFINE_FUNCTION(AWolfCharacter::execSetMaxSpeed)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_newSpeed);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetMaxSpeed(Z_Param_newSpeed);
		P_NATIVE_END;
	}
	void AWolfCharacter::StaticRegisterNativesAWolfCharacter()
	{
		UClass* Class = AWolfCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetMaxSpeed", &AWolfCharacter::execSetMaxSpeed },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics
	{
		struct WolfCharacter_eventSetMaxSpeed_Parms
		{
			float newSpeed;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_newSpeed;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::NewProp_newSpeed = { "newSpeed", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WolfCharacter_eventSetMaxSpeed_Parms, newSpeed), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::NewProp_newSpeed,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWolfCharacter, nullptr, "SetMaxSpeed", nullptr, nullptr, sizeof(Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::WolfCharacter_eventSetMaxSpeed_Parms), Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AWolfCharacter);
	UClass* Z_Construct_UClass_AWolfCharacter_NoRegister()
	{
		return AWolfCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AWolfCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TurnRateGamepad_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_TurnRateGamepad;
		static const UECodeGen_Private::FBytePropertyParams NewProp_EWolfActions_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_EWolfActions_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_EWolfActions;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsRunningAway_MetaData[];
#endif
		static void NewProp_IsRunningAway_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsRunningAway;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsWalking_MetaData[];
#endif
		static void NewProp_IsWalking_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsWalking;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsIdle_MetaData[];
#endif
		static void NewProp_IsIdle_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsIdle;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsGlaring_MetaData[];
#endif
		static void NewProp_IsGlaring_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsGlaring;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsEating_MetaData[];
#endif
		static void NewProp_IsEating_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsEating;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsAttacking_MetaData[];
#endif
		static void NewProp_IsAttacking_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsAttacking;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsResting_MetaData[];
#endif
		static void NewProp_IsResting_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsResting;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsSleeping_MetaData[];
#endif
		static void NewProp_IsSleeping_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsSleeping;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWolfCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_Deer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AWolfCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AWolfCharacter_SetMaxSpeed, "SetMaxSpeed" }, // 2151924611
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "WolfCharacter.h" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_TurnRateGamepad_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_TurnRateGamepad = { "TurnRateGamepad", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWolfCharacter, TurnRateGamepad), METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_TurnRateGamepad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_TurnRateGamepad_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_EWolfActions_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_EWolfActions_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_EWolfActions = { "EWolfActions", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWolfCharacter, EWolfActions), Z_Construct_UEnum_Deer_EAnimalAction, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_EWolfActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_EWolfActions_MetaData)) }; // 4007895198
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsRunningAway_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsRunningAway_SetBit(void* Obj)
	{
		((AWolfCharacter*)Obj)->IsRunningAway = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsRunningAway = { "IsRunningAway", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWolfCharacter), &Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsRunningAway_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsRunningAway_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsRunningAway_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsWalking_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsWalking_SetBit(void* Obj)
	{
		((AWolfCharacter*)Obj)->IsWalking = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsWalking = { "IsWalking", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWolfCharacter), &Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsWalking_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsWalking_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsWalking_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsIdle_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsIdle_SetBit(void* Obj)
	{
		((AWolfCharacter*)Obj)->IsIdle = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsIdle = { "IsIdle", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWolfCharacter), &Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsIdle_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsIdle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsIdle_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsGlaring_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsGlaring_SetBit(void* Obj)
	{
		((AWolfCharacter*)Obj)->IsGlaring = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsGlaring = { "IsGlaring", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWolfCharacter), &Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsGlaring_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsGlaring_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsGlaring_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsEating_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsEating_SetBit(void* Obj)
	{
		((AWolfCharacter*)Obj)->IsEating = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsEating = { "IsEating", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWolfCharacter), &Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsEating_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsEating_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsEating_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsAttacking_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsAttacking_SetBit(void* Obj)
	{
		((AWolfCharacter*)Obj)->IsAttacking = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsAttacking = { "IsAttacking", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWolfCharacter), &Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsAttacking_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsAttacking_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsAttacking_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsResting_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsResting_SetBit(void* Obj)
	{
		((AWolfCharacter*)Obj)->IsResting = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsResting = { "IsResting", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWolfCharacter), &Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsResting_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsResting_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsResting_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsSleeping_MetaData[] = {
		{ "Category", "WolfCharacter" },
		{ "ModuleRelativePath", "WolfCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsSleeping_SetBit(void* Obj)
	{
		((AWolfCharacter*)Obj)->IsSleeping = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsSleeping = { "IsSleeping", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWolfCharacter), &Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsSleeping_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsSleeping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsSleeping_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWolfCharacter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_TurnRateGamepad,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_EWolfActions_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_EWolfActions,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsRunningAway,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsWalking,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsIdle,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsGlaring,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsEating,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsAttacking,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsResting,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWolfCharacter_Statics::NewProp_IsSleeping,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWolfCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWolfCharacter>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AWolfCharacter_Statics::ClassParams = {
		&AWolfCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AWolfCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWolfCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWolfCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWolfCharacter()
	{
		if (!Z_Registration_Info_UClass_AWolfCharacter.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AWolfCharacter.OuterSingleton, Z_Construct_UClass_AWolfCharacter_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AWolfCharacter.OuterSingleton;
	}
	template<> DEER_API UClass* StaticClass<AWolfCharacter>()
	{
		return AWolfCharacter::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWolfCharacter);
	struct Z_CompiledInDeferFile_FID_Deer_Source_Deer_WolfCharacter_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Deer_Source_Deer_WolfCharacter_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AWolfCharacter, AWolfCharacter::StaticClass, TEXT("AWolfCharacter"), &Z_Registration_Info_UClass_AWolfCharacter, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AWolfCharacter), 1477835923U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Deer_Source_Deer_WolfCharacter_h_867723888(TEXT("/Script/Deer"),
		Z_CompiledInDeferFile_FID_Deer_Source_Deer_WolfCharacter_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Deer_Source_Deer_WolfCharacter_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
