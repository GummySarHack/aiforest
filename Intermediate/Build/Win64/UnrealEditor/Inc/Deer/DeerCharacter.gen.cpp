// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Deer/DeerCharacter.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDeerCharacter() {}
// Cross Module References
	DEER_API UClass* Z_Construct_UClass_ADeerCharacter_NoRegister();
	DEER_API UClass* Z_Construct_UClass_ADeerCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_Deer();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	DEER_API UEnum* Z_Construct_UEnum_Deer_EAnimalAction();
	AIMODULE_API UClass* Z_Construct_UClass_UGenericTeamAgentInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ADeerCharacter::execSetMaxSpeed)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_newSpeed);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetMaxSpeed(Z_Param_newSpeed);
		P_NATIVE_END;
	}
	void ADeerCharacter::StaticRegisterNativesADeerCharacter()
	{
		UClass* Class = ADeerCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetMaxSpeed", &ADeerCharacter::execSetMaxSpeed },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics
	{
		struct DeerCharacter_eventSetMaxSpeed_Parms
		{
			float newSpeed;
		};
		static const UECodeGen_Private::FFloatPropertyParams NewProp_newSpeed;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::NewProp_newSpeed = { "newSpeed", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DeerCharacter_eventSetMaxSpeed_Parms, newSpeed), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::NewProp_newSpeed,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADeerCharacter, nullptr, "SetMaxSpeed", nullptr, nullptr, sizeof(Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::DeerCharacter_eventSetMaxSpeed_Parms), Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ADeerCharacter);
	UClass* Z_Construct_UClass_ADeerCharacter_NoRegister()
	{
		return ADeerCharacter::StaticClass();
	}
	struct Z_Construct_UClass_ADeerCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TurnRateGamepad_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_TurnRateGamepad;
		static const UECodeGen_Private::FBytePropertyParams NewProp_EDeerActions_Underlying;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_EDeerActions_MetaData[];
#endif
		static const UECodeGen_Private::FEnumPropertyParams NewProp_EDeerActions;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_ID_MetaData[];
#endif
		static const UECodeGen_Private::FIntPropertyParams NewProp_ID;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsRunningAway_MetaData[];
#endif
		static void NewProp_IsRunningAway_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsRunningAway;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsGrazing_MetaData[];
#endif
		static void NewProp_IsGrazing_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsGrazing;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsWalking_MetaData[];
#endif
		static void NewProp_IsWalking_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsWalking;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsIdle_MetaData[];
#endif
		static void NewProp_IsIdle_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsIdle;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsGlaring_MetaData[];
#endif
		static void NewProp_IsGlaring_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsGlaring;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_IsEating_MetaData[];
#endif
		static void NewProp_IsEating_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_IsEating;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UECodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADeerCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_Deer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ADeerCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ADeerCharacter_SetMaxSpeed, "SetMaxSpeed" }, // 3397256123
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "DeerCharacter.h" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera behind the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeerCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Follow camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_FollowCamera = { "FollowCamera", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeerCharacter, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_FollowCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_TurnRateGamepad_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_TurnRateGamepad = { "TurnRateGamepad", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeerCharacter, TurnRateGamepad), METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_TurnRateGamepad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_TurnRateGamepad_MetaData)) };
	const UECodeGen_Private::FBytePropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_EDeerActions_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_EDeerActions_MetaData[] = {
		{ "Category", "DeerCharacter" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	const UECodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_EDeerActions = { "EDeerActions", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeerCharacter, EDeerActions), Z_Construct_UEnum_Deer_EAnimalAction, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_EDeerActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_EDeerActions_MetaData)) }; // 4007895198
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_ID_MetaData[] = {
		{ "Category", "AI" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	const UECodeGen_Private::FIntPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_ID = { "ID", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeerCharacter, ID), METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_ID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_ID_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsRunningAway_MetaData[] = {
		{ "Category", "DeerCharacter" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsRunningAway_SetBit(void* Obj)
	{
		((ADeerCharacter*)Obj)->IsRunningAway = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsRunningAway = { "IsRunningAway", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADeerCharacter), &Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsRunningAway_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsRunningAway_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsRunningAway_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGrazing_MetaData[] = {
		{ "Category", "DeerCharacter" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGrazing_SetBit(void* Obj)
	{
		((ADeerCharacter*)Obj)->IsGrazing = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGrazing = { "IsGrazing", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADeerCharacter), &Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGrazing_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGrazing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGrazing_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsWalking_MetaData[] = {
		{ "Category", "DeerCharacter" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsWalking_SetBit(void* Obj)
	{
		((ADeerCharacter*)Obj)->IsWalking = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsWalking = { "IsWalking", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADeerCharacter), &Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsWalking_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsWalking_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsWalking_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsIdle_MetaData[] = {
		{ "Category", "DeerCharacter" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsIdle_SetBit(void* Obj)
	{
		((ADeerCharacter*)Obj)->IsIdle = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsIdle = { "IsIdle", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADeerCharacter), &Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsIdle_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsIdle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsIdle_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGlaring_MetaData[] = {
		{ "Category", "DeerCharacter" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGlaring_SetBit(void* Obj)
	{
		((ADeerCharacter*)Obj)->IsGlaring = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGlaring = { "IsGlaring", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADeerCharacter), &Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGlaring_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGlaring_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGlaring_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsEating_MetaData[] = {
		{ "Category", "DeerCharacter" },
		{ "ModuleRelativePath", "DeerCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsEating_SetBit(void* Obj)
	{
		((ADeerCharacter*)Obj)->IsEating = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsEating = { "IsEating", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADeerCharacter), &Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsEating_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsEating_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsEating_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADeerCharacter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_CameraBoom,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_FollowCamera,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_TurnRateGamepad,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_EDeerActions_Underlying,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_EDeerActions,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_ID,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsRunningAway,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGrazing,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsWalking,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsIdle,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsGlaring,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerCharacter_Statics::NewProp_IsEating,
	};
		const UECodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ADeerCharacter_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UGenericTeamAgentInterface_NoRegister, (int32)VTABLE_OFFSET(ADeerCharacter, IGenericTeamAgentInterface), false },  // 3302830175
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADeerCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADeerCharacter>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ADeerCharacter_Statics::ClassParams = {
		&ADeerCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ADeerCharacter_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADeerCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADeerCharacter()
	{
		if (!Z_Registration_Info_UClass_ADeerCharacter.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ADeerCharacter.OuterSingleton, Z_Construct_UClass_ADeerCharacter_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ADeerCharacter.OuterSingleton;
	}
	template<> DEER_API UClass* StaticClass<ADeerCharacter>()
	{
		return ADeerCharacter::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADeerCharacter);
	struct Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerCharacter_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerCharacter_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ADeerCharacter, ADeerCharacter::StaticClass, TEXT("ADeerCharacter"), &Z_Registration_Info_UClass_ADeerCharacter, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ADeerCharacter), 2074472737U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerCharacter_h_1397811395(TEXT("/Script/Deer"),
		Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerCharacter_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerCharacter_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
