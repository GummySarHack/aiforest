// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DEER_WolfCharacter_generated_h
#error "WolfCharacter.generated.h already included, missing '#pragma once' in WolfCharacter.h"
#endif
#define DEER_WolfCharacter_generated_h

#define FID_Deer_Source_Deer_WolfCharacter_h_14_SPARSE_DATA
#define FID_Deer_Source_Deer_WolfCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetMaxSpeed);


#define FID_Deer_Source_Deer_WolfCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetMaxSpeed);


#define FID_Deer_Source_Deer_WolfCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWolfCharacter(); \
	friend struct Z_Construct_UClass_AWolfCharacter_Statics; \
public: \
	DECLARE_CLASS(AWolfCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Deer"), NO_API) \
	DECLARE_SERIALIZER(AWolfCharacter)


#define FID_Deer_Source_Deer_WolfCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAWolfCharacter(); \
	friend struct Z_Construct_UClass_AWolfCharacter_Statics; \
public: \
	DECLARE_CLASS(AWolfCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Deer"), NO_API) \
	DECLARE_SERIALIZER(AWolfCharacter)


#define FID_Deer_Source_Deer_WolfCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWolfCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWolfCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWolfCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWolfCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWolfCharacter(AWolfCharacter&&); \
	NO_API AWolfCharacter(const AWolfCharacter&); \
public:


#define FID_Deer_Source_Deer_WolfCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWolfCharacter(AWolfCharacter&&); \
	NO_API AWolfCharacter(const AWolfCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWolfCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWolfCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWolfCharacter)


#define FID_Deer_Source_Deer_WolfCharacter_h_11_PROLOG
#define FID_Deer_Source_Deer_WolfCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Deer_Source_Deer_WolfCharacter_h_14_SPARSE_DATA \
	FID_Deer_Source_Deer_WolfCharacter_h_14_RPC_WRAPPERS \
	FID_Deer_Source_Deer_WolfCharacter_h_14_INCLASS \
	FID_Deer_Source_Deer_WolfCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Deer_Source_Deer_WolfCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Deer_Source_Deer_WolfCharacter_h_14_SPARSE_DATA \
	FID_Deer_Source_Deer_WolfCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Deer_Source_Deer_WolfCharacter_h_14_INCLASS_NO_PURE_DECLS \
	FID_Deer_Source_Deer_WolfCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DEER_API UClass* StaticClass<class AWolfCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Deer_Source_Deer_WolfCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
