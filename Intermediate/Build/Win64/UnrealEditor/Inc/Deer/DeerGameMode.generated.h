// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DEER_DeerGameMode_generated_h
#error "DeerGameMode.generated.h already included, missing '#pragma once' in DeerGameMode.h"
#endif
#define DEER_DeerGameMode_generated_h

#define FID_Deer_Source_Deer_DeerGameMode_h_12_SPARSE_DATA
#define FID_Deer_Source_Deer_DeerGameMode_h_12_RPC_WRAPPERS
#define FID_Deer_Source_Deer_DeerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Deer_Source_Deer_DeerGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADeerGameMode(); \
	friend struct Z_Construct_UClass_ADeerGameMode_Statics; \
public: \
	DECLARE_CLASS(ADeerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Deer"), DEER_API) \
	DECLARE_SERIALIZER(ADeerGameMode)


#define FID_Deer_Source_Deer_DeerGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesADeerGameMode(); \
	friend struct Z_Construct_UClass_ADeerGameMode_Statics; \
public: \
	DECLARE_CLASS(ADeerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Deer"), DEER_API) \
	DECLARE_SERIALIZER(ADeerGameMode)


#define FID_Deer_Source_Deer_DeerGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DEER_API ADeerGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADeerGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DEER_API, ADeerGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeerGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DEER_API ADeerGameMode(ADeerGameMode&&); \
	DEER_API ADeerGameMode(const ADeerGameMode&); \
public:


#define FID_Deer_Source_Deer_DeerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DEER_API ADeerGameMode(ADeerGameMode&&); \
	DEER_API ADeerGameMode(const ADeerGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DEER_API, ADeerGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeerGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADeerGameMode)


#define FID_Deer_Source_Deer_DeerGameMode_h_9_PROLOG
#define FID_Deer_Source_Deer_DeerGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Deer_Source_Deer_DeerGameMode_h_12_SPARSE_DATA \
	FID_Deer_Source_Deer_DeerGameMode_h_12_RPC_WRAPPERS \
	FID_Deer_Source_Deer_DeerGameMode_h_12_INCLASS \
	FID_Deer_Source_Deer_DeerGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Deer_Source_Deer_DeerGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Deer_Source_Deer_DeerGameMode_h_12_SPARSE_DATA \
	FID_Deer_Source_Deer_DeerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Deer_Source_Deer_DeerGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FID_Deer_Source_Deer_DeerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DEER_API UClass* StaticClass<class ADeerGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Deer_Source_Deer_DeerGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
