// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDeer_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_Deer;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_Deer()
	{
		if (!Z_Registration_Info_UPackage__Script_Deer.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/Deer",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0x65FE85FC,
				0x6F9F3669,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_Deer.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_Deer.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_Deer(Z_Construct_UPackage__Script_Deer, TEXT("/Script/Deer"), Z_Registration_Info_UPackage__Script_Deer, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x65FE85FC, 0x6F9F3669));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
