// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Deer/DeerGameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDeerGameMode() {}
// Cross Module References
	DEER_API UClass* Z_Construct_UClass_ADeerGameMode_NoRegister();
	DEER_API UClass* Z_Construct_UClass_ADeerGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Deer();
// End Cross Module References
	void ADeerGameMode::StaticRegisterNativesADeerGameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ADeerGameMode);
	UClass* Z_Construct_UClass_ADeerGameMode_NoRegister()
	{
		return ADeerGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ADeerGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADeerGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Deer,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "DeerGameMode.h" },
		{ "ModuleRelativePath", "DeerGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADeerGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADeerGameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ADeerGameMode_Statics::ClassParams = {
		&ADeerGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ADeerGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADeerGameMode()
	{
		if (!Z_Registration_Info_UClass_ADeerGameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ADeerGameMode.OuterSingleton, Z_Construct_UClass_ADeerGameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ADeerGameMode.OuterSingleton;
	}
	template<> DEER_API UClass* StaticClass<ADeerGameMode>()
	{
		return ADeerGameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADeerGameMode);
	struct Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerGameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerGameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ADeerGameMode, ADeerGameMode::StaticClass, TEXT("ADeerGameMode"), &Z_Registration_Info_UClass_ADeerGameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ADeerGameMode), 2276294904U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerGameMode_h_2538119918(TEXT("/Script/Deer"),
		Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerGameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerGameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
