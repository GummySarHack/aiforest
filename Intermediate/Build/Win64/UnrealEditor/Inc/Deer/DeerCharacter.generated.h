// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DEER_DeerCharacter_generated_h
#error "DeerCharacter.generated.h already included, missing '#pragma once' in DeerCharacter.h"
#endif
#define DEER_DeerCharacter_generated_h

#define FID_Deer_Source_Deer_DeerCharacter_h_17_SPARSE_DATA
#define FID_Deer_Source_Deer_DeerCharacter_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetMaxSpeed);


#define FID_Deer_Source_Deer_DeerCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetMaxSpeed);


#define FID_Deer_Source_Deer_DeerCharacter_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADeerCharacter(); \
	friend struct Z_Construct_UClass_ADeerCharacter_Statics; \
public: \
	DECLARE_CLASS(ADeerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Deer"), NO_API) \
	DECLARE_SERIALIZER(ADeerCharacter) \
	virtual UObject* _getUObject() const override { return const_cast<ADeerCharacter*>(this); }


#define FID_Deer_Source_Deer_DeerCharacter_h_17_INCLASS \
private: \
	static void StaticRegisterNativesADeerCharacter(); \
	friend struct Z_Construct_UClass_ADeerCharacter_Statics; \
public: \
	DECLARE_CLASS(ADeerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Deer"), NO_API) \
	DECLARE_SERIALIZER(ADeerCharacter) \
	virtual UObject* _getUObject() const override { return const_cast<ADeerCharacter*>(this); }


#define FID_Deer_Source_Deer_DeerCharacter_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADeerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADeerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADeerCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADeerCharacter(ADeerCharacter&&); \
	NO_API ADeerCharacter(const ADeerCharacter&); \
public:


#define FID_Deer_Source_Deer_DeerCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADeerCharacter(ADeerCharacter&&); \
	NO_API ADeerCharacter(const ADeerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADeerCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADeerCharacter)


#define FID_Deer_Source_Deer_DeerCharacter_h_13_PROLOG
#define FID_Deer_Source_Deer_DeerCharacter_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Deer_Source_Deer_DeerCharacter_h_17_SPARSE_DATA \
	FID_Deer_Source_Deer_DeerCharacter_h_17_RPC_WRAPPERS \
	FID_Deer_Source_Deer_DeerCharacter_h_17_INCLASS \
	FID_Deer_Source_Deer_DeerCharacter_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Deer_Source_Deer_DeerCharacter_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Deer_Source_Deer_DeerCharacter_h_17_SPARSE_DATA \
	FID_Deer_Source_Deer_DeerCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Deer_Source_Deer_DeerCharacter_h_17_INCLASS_NO_PURE_DECLS \
	FID_Deer_Source_Deer_DeerCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DEER_API UClass* StaticClass<class ADeerCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Deer_Source_Deer_DeerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
