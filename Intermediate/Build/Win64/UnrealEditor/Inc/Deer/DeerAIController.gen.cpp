// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Deer/DeerAIController.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDeerAIController() {}
// Cross Module References
	DEER_API UClass* Z_Construct_UClass_ADeerAIController_NoRegister();
	DEER_API UClass* Z_Construct_UClass_ADeerAIController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_Deer();
	AIMODULE_API UClass* Z_Construct_UClass_UAIPerceptionComponent_NoRegister();
	DEER_API UClass* Z_Construct_UClass_ADeerCharacter_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void ADeerAIController::StaticRegisterNativesADeerAIController()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ADeerAIController);
	UClass* Z_Construct_UClass_ADeerAIController_NoRegister()
	{
		return ADeerAIController::StaticClass();
	}
	struct Z_Construct_UClass_ADeerAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AIPerceptionComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_AIPerceptionComponent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Loop_MetaData[];
#endif
		static void NewProp_Loop_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_Loop;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Deer_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Deer;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DetectedPlayer_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_DetectedPlayer;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADeerAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_Deer,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerAIController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Transformation" },
		{ "IncludePath", "DeerAIController.h" },
		{ "ModuleRelativePath", "DeerAIController.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerAIController_Statics::NewProp_AIPerceptionComponent_MetaData[] = {
		{ "Category", "DeerAIController" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "DeerAIController.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADeerAIController_Statics::NewProp_AIPerceptionComponent = { "AIPerceptionComponent", nullptr, (EPropertyFlags)0x001000000008000d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeerAIController, AIPerceptionComponent), Z_Construct_UClass_UAIPerceptionComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADeerAIController_Statics::NewProp_AIPerceptionComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerAIController_Statics::NewProp_AIPerceptionComponent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerAIController_Statics::NewProp_Loop_MetaData[] = {
		{ "Category", "DeerAIController" },
		{ "Comment", "/* VARIABLES */" },
		{ "ModuleRelativePath", "DeerAIController.h" },
		{ "ToolTip", "VARIABLES" },
	};
#endif
	void Z_Construct_UClass_ADeerAIController_Statics::NewProp_Loop_SetBit(void* Obj)
	{
		((ADeerAIController*)Obj)->Loop = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADeerAIController_Statics::NewProp_Loop = { "Loop", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADeerAIController), &Z_Construct_UClass_ADeerAIController_Statics::NewProp_Loop_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADeerAIController_Statics::NewProp_Loop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerAIController_Statics::NewProp_Loop_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerAIController_Statics::NewProp_Deer_MetaData[] = {
		{ "Category", "DeerAIController" },
		{ "ModuleRelativePath", "DeerAIController.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADeerAIController_Statics::NewProp_Deer = { "Deer", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeerAIController, Deer), Z_Construct_UClass_ADeerCharacter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADeerAIController_Statics::NewProp_Deer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerAIController_Statics::NewProp_Deer_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADeerAIController_Statics::NewProp_DetectedPlayer_MetaData[] = {
		{ "Category", "DeerAIController" },
		{ "ModuleRelativePath", "DeerAIController.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADeerAIController_Statics::NewProp_DetectedPlayer = { "DetectedPlayer", nullptr, (EPropertyFlags)0x0010000000000004, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADeerAIController, DetectedPlayer), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADeerAIController_Statics::NewProp_DetectedPlayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerAIController_Statics::NewProp_DetectedPlayer_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADeerAIController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerAIController_Statics::NewProp_AIPerceptionComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerAIController_Statics::NewProp_Loop,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerAIController_Statics::NewProp_Deer,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADeerAIController_Statics::NewProp_DetectedPlayer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADeerAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADeerAIController>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ADeerAIController_Statics::ClassParams = {
		&ADeerAIController::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ADeerAIController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ADeerAIController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADeerAIController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADeerAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADeerAIController()
	{
		if (!Z_Registration_Info_UClass_ADeerAIController.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ADeerAIController.OuterSingleton, Z_Construct_UClass_ADeerAIController_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ADeerAIController.OuterSingleton;
	}
	template<> DEER_API UClass* StaticClass<ADeerAIController>()
	{
		return ADeerAIController::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADeerAIController);
	struct Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerAIController_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerAIController_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ADeerAIController, ADeerAIController::StaticClass, TEXT("ADeerAIController"), &Z_Registration_Info_UClass_ADeerAIController, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ADeerAIController), 3600625438U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerAIController_h_2334567820(TEXT("/Script/Deer"),
		Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerAIController_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Deer_Source_Deer_DeerAIController_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
