// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DEER_DeerAIController_generated_h
#error "DeerAIController.generated.h already included, missing '#pragma once' in DeerAIController.h"
#endif
#define DEER_DeerAIController_generated_h

#define FID_Deer_Source_Deer_DeerAIController_h_17_SPARSE_DATA
#define FID_Deer_Source_Deer_DeerAIController_h_17_RPC_WRAPPERS
#define FID_Deer_Source_Deer_DeerAIController_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Deer_Source_Deer_DeerAIController_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADeerAIController(); \
	friend struct Z_Construct_UClass_ADeerAIController_Statics; \
public: \
	DECLARE_CLASS(ADeerAIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Deer"), NO_API) \
	DECLARE_SERIALIZER(ADeerAIController)


#define FID_Deer_Source_Deer_DeerAIController_h_17_INCLASS \
private: \
	static void StaticRegisterNativesADeerAIController(); \
	friend struct Z_Construct_UClass_ADeerAIController_Statics; \
public: \
	DECLARE_CLASS(ADeerAIController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Deer"), NO_API) \
	DECLARE_SERIALIZER(ADeerAIController)


#define FID_Deer_Source_Deer_DeerAIController_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADeerAIController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADeerAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADeerAIController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeerAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADeerAIController(ADeerAIController&&); \
	NO_API ADeerAIController(const ADeerAIController&); \
public:


#define FID_Deer_Source_Deer_DeerAIController_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADeerAIController(ADeerAIController&&); \
	NO_API ADeerAIController(const ADeerAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADeerAIController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADeerAIController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADeerAIController)


#define FID_Deer_Source_Deer_DeerAIController_h_14_PROLOG
#define FID_Deer_Source_Deer_DeerAIController_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Deer_Source_Deer_DeerAIController_h_17_SPARSE_DATA \
	FID_Deer_Source_Deer_DeerAIController_h_17_RPC_WRAPPERS \
	FID_Deer_Source_Deer_DeerAIController_h_17_INCLASS \
	FID_Deer_Source_Deer_DeerAIController_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Deer_Source_Deer_DeerAIController_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Deer_Source_Deer_DeerAIController_h_17_SPARSE_DATA \
	FID_Deer_Source_Deer_DeerAIController_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Deer_Source_Deer_DeerAIController_h_17_INCLASS_NO_PURE_DECLS \
	FID_Deer_Source_Deer_DeerAIController_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DEER_API UClass* StaticClass<class ADeerAIController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Deer_Source_Deer_DeerAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
