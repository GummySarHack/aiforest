#pragma once
#pragma once

#include "CoreMinimal.h"
#include "AnimalTags.generated.h"

UENUM(BlueprintType)
enum class EAnimalTags : uint8
{
	PREDATOR,
	OBSERVER
};