# AIForest



## Getting started

This is an project to create a forest where a player interacts with AIs.


***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
* Create a deer AI
* Deer AI walks away from player when approching
* Deer AI walks around, sometimes eats and look around
* Deer AI waits and observes player is he's at certain distance
	* either player gets out of sigth
	* either player gets to close and Deer AI runs away